const domHtml = document.querySelector('html');
const focoButton = document.querySelector('.app__card-button--foco');
const curtoButton = document.querySelector('.app__card-button--curto');
const longoButton = document.querySelector('.app__card-button--longo');
const banner = document.querySelector('.app__image');
const title = document.querySelector('.app__title');
const botoes = document.querySelectorAll('.app__card-button');

const tempoNaTela = document.querySelector('#timer');

const startPauseBt = document.querySelector('#start-pause');
const iniciarOuPausarBt = document.querySelector('#start-pause span');
const startPauseBtIcon = document.querySelector('#start-pause img');

const audioZerar = new Audio('/sons/beep.mp3');
const audioPlay = new Audio('/sons/play.wav');
const audioPause = new Audio('/sons/pause.mp3');

const musicaToggle = document.querySelector('#alternar-musica');
const musica = new Audio('/sons/luna-rise-part-one.mp3');
musica.loop = true;

let tempoDecorridoEmSegundos = 1500;
let intervaloId = null;

musicaToggle.addEventListener( 'change', () => {
    if (musica.paused) {
        musica.play();
    } else {
        musica.pause();
    }
});

focoButton.addEventListener('click', () => {
    tempoDecorridoEmSegundos = 1500;
    alterarContexto('foco');
    focoButton.classList.add('active');
});

curtoButton.addEventListener('click', () => {
    tempoDecorridoEmSegundos = 300;
    alterarContexto('descanso-curto');
    curtoButton.classList.add('active');
});

longoButton.addEventListener('click', () => {
    tempoDecorridoEmSegundos = 900;
    alterarContexto('descanso-longo');
    longoButton.classList.add('active');
});

function alterarContexto(contexto) {
    mostrarTempo();
    botoes.forEach( function (contexto) {
        contexto.classList.remove('active');
    });
    domHtml.setAttribute('data-contexto', contexto);
    banner.setAttribute('src', `/imagens/${contexto}.png`);
    switch (contexto) {
        case 'foco':
            title.innerHTML = `Otimize sua produtividade,<br>
            <strong class="app__title-strong">mergulhe no que importa.</strong>`;
            break;
        case 'descanso-curto':
            title.innerHTML = `Que tal dar uma respirada? <strong class="app__title-strong">Faça uma pausa curta!</strong>`;
            break;
        case 'descanso-longo':
            title.innerHTML = `Hora de voltar à superfície.<strong class="app__title-strong"> Faça uma pausa longa.</strong>`;
            break;
    }
}

const contagemRegressiva = () => {
    if ( tempoDecorridoEmSegundos <= 0 ) {
        audioZerar.play();
        alert('Tempo Finalizado');
        zerar();
        return;
    }
    tempoDecorridoEmSegundos -= 1;
    mostrarTempo();
}

startPauseBt.addEventListener( 'click', iniciarPausar );

function iniciarPausar() {
    if ( intervaloId ) {
        audioPause.play();
        zerar();
        return;
    }
    audioPlay.play();
    intervaloId = setInterval(contagemRegressiva, 1000);
    iniciarOuPausarBt.textContent = "Pausar";
    startPauseBtIcon.setAttribute('src', '/imagens/pause.png');
}

function zerar () {
    clearInterval(intervaloId);
    intervaloId = null;
    iniciarOuPausarBt.textContent = "Começar";
    startPauseBtIcon.setAttribute('src', '/imagens/play_arrow.png');
}

function mostrarTempo () {
    const tempo = new Date( tempoDecorridoEmSegundos * 1000 );
    const tempoFormatado = tempo.toLocaleTimeString('pt-Br', {minute: '2-digit', second: '2-digit'});
    tempoNaTela.innerHTML = `${tempoFormatado}`;
}

mostrarTempo();